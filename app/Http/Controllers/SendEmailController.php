<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Mail\SendMail;
class SendEmailController extends Controller
{
    //
    function index()
    {
        return view('welcome');
    }

    function send(Request $request)
    {
        $data = array(
            'name' => $request->name,
            'message' => $request->message,
            'service' => $request->service,
            'deadline' => $request->deadline
        );

        Mail::to('fedde198212@gmail.com')->send(new SendMail($data));

        return back()->with('success', 'Thanks for contacting us!');
    }
}
