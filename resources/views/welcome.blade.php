<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Profile</title>

    <!-- Bootstrap core CSS -->
    <link href="./vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="./vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="./css/agency.css" rel="stylesheet">
    <link href="./css/agency.min.css" rel="stylesheet">

    {{-- <link rel="shortcut icon" type="image/ico" href="favicon.ico"/>
    <link rel="shortcut icon" type="image/png" href="favicon.png"/> --}}
    {{-- <link href="dist/css/slick.css" rel="stylesheet" type="text/css" />--}}
    <link rel="stylesheet" href="./dist/css/app.css" type="text/css" media="all" />
  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">Home</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav text-uppercase ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#services">Services</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#process">Process</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#team">About us</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Header -->
    <header class="masthead" style="min-height: 940px;">
      <div class="container">
        <div class="intro-text">
          <div class="intro-lead-in">Welcome To Our Website!</div>
          <div class="intro-heading text-uppercase">PARALLAX EXPERIENCE</div>
          <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#services">Get in Touch</a>
        </div>
      </div>
    </header>

    <!-- Services -->
    <section id="services" style="height:830px;">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center" style="margin-bottom:50px;">
            <h2 class="section-heading text-uppercase" style="margin-bottom:50px;">Services</h2>
          </div>
        </div>
        <div class="row text-center">
          <div class="col-md-3 servicecls" data-toggle="modal" data-target="#serviceModal1">
            <span class="fa-stack fa-4x">
              <i class="fas fa-circle fa-stack-2x text-primary"></i>
              <i class="fas fa-shopping-cart fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Web Development</h4>
            <ul class="text-left">
                <li >Website Design</li>
                <li >Mobile App Design</li>
                <li >Branding</li>
                <li >Web App Design</li>
            </ul>
            <p id="overlay">Show Portfolio</p>
          </div>
          <div class="col-md-3 servicecls" data-toggle="modal" data-target="#serviceModal2">
            <span class="fa-stack fa-4x">
              <i class="fas fa-circle fa-stack-2x text-primary"></i>
              <i class="fas fa-laptop fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Video Animation</h4>
            <ul class="text-left">
                <li >Website Design</li>
                <li >Mobile App Design</li>
                <li >Branding</li>
                <li >Web App Design</li>
            </ul>
             </div>
          <div class="col-md-3 servicecls" data-toggle="modal" data-target="#serviceModal3">
            <span class="fa-stack fa-4x">
              <i class="fas fa-circle fa-stack-2x text-primary"></i>
              <i class="fas fa-lock fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Editing</h4>
            <ul class="text-left">
                <li >Website Design</li>
                <li >Mobile App Design</li>
                <li >Branding</li>
                <li >Web App Design</li>
            </ul>
             </div>
          <div class="col-md-3 servicecls" data-toggle="modal" data-target="#serviceModal4">
            <span class="fa-stack fa-4x">
              <i class="fas fa-circle fa-stack-2x text-primary"></i>
              <i class="fas fa-lock fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Graphic Design</h4>
            <ul class="text-left">
                <li >Website Design</li>
                <li >Mobile App Design</li>
                <li >Branding</li>
                <li >Web App Design</li>
            </ul>
            </div>
        </div>
      </div>
    </section>

    <!-- Portfolio Grid -->
    <section class="bg-light" id="portfolio" style="padding-top:100px;">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Latest Work</h2>
          </div>
        </div>
        <div class="row">
          {{-- https://www.youtube.com/watch?v=Bvme20glf_A --}}
         {{-- <div class="col-md-4 col-sm-6" data-target="#video-7" data-toggle="modal">
          <div class="projects__image" style="background-image: url('img/portfolio/04-thumbnail.jpg')">
          <div class="clients-video__overlay small-overlay"></div>
          </div>
          </div>
          <div class="col-md-4 col-sm-6" data-target="#video-8" data-toggle="modal">
          <div class="projects__image" style="background-image: url('img/portfolio/05-thumbnail.jpg')">
          <div class="clients-video__overlay small-overlay"></div>
          </div>
          </div>
          <div class="col-md-4 col-sm-6" data-target="#video-9" data-toggle="modal">
          <div class="projects__image" style="background-image: url('img/portfolio/06-thumbnail.jpg')">
          <div class="clients-video__overlay small-overlay"></div>
          </div>
      </div>     --}}
{{-- <iframe width="854" height="480" src="https://www.youtube.com/watch?v=CJ88Z-rMDEI" frameborder="0" allowfullscreen></iframe> --}}
        <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal4">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fas fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="./img/portfolio/04-thumbnail.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Whiteboard video</h4>
              <p class="text-muted">Video Editor</p>
            </div>
          </div>
          {{-- https://www.youtube.com/watch?v=4044So6wlqo --}}
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal5">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fas fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="./img/portfolio/05-thumbnail.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Explainers</h4>
              <p class="text-muted">Video Editor</p>
            </div>
          </div>
          {{-- https://www.youtube.com/watch?v=TJaIXRNJD0E  --}}
           <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal6">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fas fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="./img/portfolio/06-thumbnail.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Technology Video</h4>
              <p class="text-muted">Video Editor</p>
            </div>
          </div>


                <div class="col-md-4 col-sm-6 portfolio-item">
                  <a class="portfolio-link" data-toggle="modal" href="#portfolioModal1">
                    <div class="portfolio-hover">
                      <div class="portfolio-hover-content">
                        <i class="fas fa-plus fa-3x"></i>
                      </div>
                    </div>
                    <img class="img-fluid" src="./img/portfolio/01-thumbnail.jpg" alt="">
                  </a>
                  <div class="portfolio-caption">
                    <h4>Color retouching</h4>
                    <p class="text-muted">Graphic Design</p>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                  <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
                    <div class="portfolio-hover">
                      <div class="portfolio-hover-content">
                        <i class="fas fa-plus fa-3x"></i>
                      </div>
                    </div>
                    <img class="img-fluid" src="./img/portfolio/02-thumbnail.jpg" alt="">
                  </a>
                  <div class="portfolio-caption">
                    <h4>Logo Design</h4>
                    <p class="text-muted">Graphic Design</p>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                  <a class="portfolio-link" data-toggle="modal" href="#portfolioModal3">
                    <div class="portfolio-hover">
                      <div class="portfolio-hover-content">
                        <i class="fas fa-plus fa-3x"></i>
                      </div>
                    </div>
                    <img class="img-fluid" src="./img/portfolio/03-thumbnail.jpg" alt="">
                  </a>
                  <div class="portfolio-caption">
                    <h4>Fly Design</h4>
                    <p class="text-muted">Brochure Design</p>
                  </div>
                </div>
        </div>
      </div>
    </section>
    <section class="process" id="process" style="
        background-color: #fff;
        background-image: url(./img/process.jpg);
        background-repeat: repeat-x;
        background-position: center center;
        background-size: cover;
        border-top: 1px solid #646464;
        padding-top:30px;
        color: #fff;">
      <div class="container-fluid">
        <div class="row" style="display: inline-block;text-align: center;width: 100%;padding-top:-20px;">
          <h1>   Step by Step,</h1>
          <br>
          <h1>   How do we make videos?</h1>
          <br>
          <div>
          <div class="process-container">
            <img src="./dist/images/process-wheel-map.png" usemap="#wheel-map" alt="Process Wheel" class="img-responsive" />
            <div class="process-content">
                <div class="default active">
                  <img src="./dist/images/process-default.png" class="img-responsive" />
                </div>
                <div class="point-1">Questions, research, kickoff calls, concepting - all that fun stuff.</div>
                <div class="point-2">Taking those ideas we built in the concepting, and building on them more!</div>
                <div class="point-3">Creating the visual narrative for your script, bringing it to life with sketches</div>
                <div class="point-4">Custom auditions to find the PERFECT voice for your brand </div>
                <div class="point-5">Time to turn those sketches into beautiful artwork.  Give us a few days :) </div>
                <div class="point-6">The magic happens here.  Mixing everything together in animation.</div>
                <div class="point-7">Creating more ambience in your video by customizing the music and sound bed.</div>
                <div class="point-8">Need any last renders, revisions before delivery?  Here’s your chance.</div>
              </div>
  				<map name="wheel-map" id="process-wheel-map">
  					<area id="point-1" alt="" title="point-1" href="#" shape="rect" coords="442,0,560,112" style="outline:none;" target="_self" />
  					<area id="point-2" alt="" title="point-2" href="#" shape="rect" coords="583,140,701,252" style="outline:none;" target="_self" />
  					<area id="point-3" alt="" title="point-3" href="#" shape="rect" coords="580,324,698,436" style="outline:none;" target="_self" />
  					<area id="point-4" alt="" title="point-4" href="#" shape="rect" coords="432,460,550,572" style="outline:none;" target="_self" />
  					<area id="point-5" alt="" title="point-5" href="#" shape="rect" coords="236,462,354,574" style="outline:none;" target="_self" />
  					<area id="point-6" alt="" title="point-6" href="#" shape="rect" coords="85,337,203,449" style="outline:none;" target="_self" />
  					<area id="point-7" alt="" title="point-7" href="#" shape="rect" coords="58,148,176,260" style="outline:none;" target="_self" />
  					<area id="point-8" alt="" title="point-8" href="#" shape="rect" coords="203,0,321,112" style="outline:none;" target="_self"/>
  				</map>
        </div>
      </div>
    </section>

    <section class="bg-light" id="team" style="padding-top:50px; background-size: cover;background-image:url(./img/about/44.jpg); background-repeat: no-repeat;background-position: center;">
      <div class="container" style="
            max-width: 1800px;
            padding-right: 15px;
            padding-left: 50px;
            margin-right: 30px;
            margin-left: 30px;">
        <div class="row">
          <div class="col-lg-12 text-center">
              <center>
                  <h2 class="section-heading text-uppercase">About us</h2>
              </center>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-2">
            <div class="row team-member">
                <center>
                  <img class="mx-auto rounded-circle" src="./img/team/1.jpg" alt="" style="width:170px;height:170px;">
                  <h4>Matthijs</h4>
                  <p class="text-muted">Video Animation Developer</p>
                </center>
            </div>
            <div class="row team-member" >
                <center>
                  <img class="mx-auto rounded-circle" src="./img/team/2.jpg" alt=""  style="width:170px;height:170px;">
                  <h4>Danieles</h4>
                  <p class="text-muted">Graphic Designer</p>
                </center>
            </div>
          </div>
              <div class="col-sm-6 aboutus">
                  <div style="border:1px solid black;width:95%;height:80%;padding:30px;">
                      <p> This is service section.</p>
                </div>
            </div>
            <div class="col-sm-4">
              <div style="border:1px solid black;width:95%;height:80%;padding:30px;">
                  <p> This is review section.</p>
              </div>
            </div>
        </div>
      </div>
    </section>

    <!-- Clients -->
    {{-- <section class="py-5">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-6">
            <a href="#">
              <img class="img-fluid d-block mx-auto" src="img/logos/envato.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-sm-6">
            <a href="#">
              <img class="img-fluid d-block mx-auto" src="img/logos/designmodo.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-sm-6">
            <a href="#">
              <img class="img-fluid d-block mx-auto" src="img/logos/themeforest.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-sm-6">
            <a href="#">
              <img class="img-fluid d-block mx-auto" src="img/logos/creative-market.jpg" alt="">
            </a>
          </div>
        </div>
      </div>
    </section> --}}


    <!-- Contact -->
    <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Contact Us</h2>
            <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
              {{-- @if($message = Session::get('success'))
                  <div class="alert alert-success alert-bolco">
                      <button type="button" class="close" data-dismiss="alert">x</button>
                      <strong>{{$message}}</strong>
                  </div> --}}
            <form method="post" action="{{ url('sendemail/send')}}" id="contactForm" name="sentMessage" novalidate="novalidate">
                {{csrf_field()}}
             <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input class="form-control" name="name" type="text" placeholder="Your Name *" required="required" data-validation-required-message="Please enter your name.">
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <input class="form-control" name="email" type="email" placeholder="Your Email *" required="required" data-validation-required-message="Please enter your email address.">
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                      <select class="form-control" name="service" placeholder="Your Services" style="height: calc(2.25rem + 30px);disabled-color: gray;" required="required" data-validation-required-message="Please select your service.">
                          <option style="color:gray" disabled selected hidden>Your Service*</option>
                          <option value="Web Development">Web Development</option>
                          <option value="Video Animation">Video Animation</option>
                          <option value="Editing">Editing</option>
                          <option value="Graphic Design">Graphic Design</option>
                        </select>
                  </div>
                  <div class="form-group">
                    <input class="form-control" name="deadline" type="text" placeholder="Your Deadline *" required="required" data-validation-required-message="Please enter your deadline.">
                    <p class="help-block text-danger"></p>
                  </div>

                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <textarea class="form-control" name="message" placeholder="Your Message *" required="required" data-validation-required-message="Please enter a message."></textarea>
                    <p class="help-block text-danger"></p>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-12 text-center">
                  <div id="success"></div>
                  <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase" type="submit">Send Message</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>

    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <span class="copyright">Copyright &copy; Your Website 2018</span>
          </div>
          <div class="col-md-4">
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-facebook-f"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-linkedin-in"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="col-md-4">
            <ul class="list-inline quicklinks">
              <li class="list-inline-item">
                <a href="#">Privacy Policy</a>
              </li>
              <li class="list-inline-item">
                <a href="#">Terms of Use</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
    <!-- Services Modals -->
    <div class="portfolio-modal modal fade" id="serviceModal1" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-12 mx-auto">
                <div class="modal-body">
                 <iframe width="300" height="200" src="https://www.youtube.com/embed/lc5E2oC0Ab8" frameborder="0" allowfullscreen></iframe>
                 <iframe width="300" height="200" src="https://www.youtube.com/embed/lc5E2oC0Ab8" frameborder="0" allowfullscreen></iframe>
                 <iframe width="300" height="200" src="https://www.youtube.com/embed/lc5E2oC0Ab8" frameborder="0" allowfullscreen></iframe>
                 <iframe width="300" height="200" src="https://www.youtube.com/embed/lc5E2oC0Ab8" frameborder="0" allowfullscreen></iframe>
                 <iframe width="300" height="200" src="https://www.youtube.com/embed/lc5E2oC0Ab8" frameborder="0" allowfullscreen></iframe>
                 <iframe width="300" height="200" src="https://www.youtube.com/embed/lc5E2oC0Ab8" frameborder="0" allowfullscreen></iframe>
                 <br>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fas fa-times"></i>
                    Close Project</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="portfolio-modal modal fade" id="serviceModal2" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-12 mx-auto">
                <div class="modal-body">
                 <iframe width="300" height="200" src="https://www.youtube.com/embed/lc5E2oC0Ab8" frameborder="0" allowfullscreen></iframe>
                 <iframe width="300" height="200" src="https://www.youtube.com/embed/lc5E2oC0Ab8" frameborder="0" allowfullscreen></iframe>
                 <iframe width="300" height="200" src="https://www.youtube.com/embed/lc5E2oC0Ab8" frameborder="0" allowfullscreen></iframe>
                 <iframe width="300" height="200" src="https://www.youtube.com/embed/lc5E2oC0Ab8" frameborder="0" allowfullscreen></iframe>
                 <iframe width="300" height="200" src="https://www.youtube.com/embed/lc5E2oC0Ab8" frameborder="0" allowfullscreen></iframe>
                 <iframe width="300" height="200" src="https://www.youtube.com/embed/lc5E2oC0Ab8" frameborder="0" allowfullscreen></iframe>
                 <br>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fas fa-times"></i>
                    Close Project</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="portfolio-modal modal fade" id="serviceModal3" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-12 mx-auto">
                <div class="modal-body">
                 <iframe width="300" height="200" src="https://www.youtube.com/embed/lc5E2oC0Ab8" frameborder="0" allowfullscreen></iframe>
                 <iframe width="300" height="200" src="https://www.youtube.com/embed/lc5E2oC0Ab8" frameborder="0" allowfullscreen></iframe>
                 <iframe width="300" height="200" src="https://www.youtube.com/embed/lc5E2oC0Ab8" frameborder="0" allowfullscreen></iframe>
                 <iframe width="300" height="200" src="https://www.youtube.com/embed/lc5E2oC0Ab8" frameborder="0" allowfullscreen></iframe>
                 <iframe width="300" height="200" src="https://www.youtube.com/embed/lc5E2oC0Ab8" frameborder="0" allowfullscreen></iframe>
                 <iframe width="300" height="200" src="https://www.youtube.com/embed/lc5E2oC0Ab8" frameborder="0" allowfullscreen></iframe>
                 <br>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fas fa-times"></i>
                    Close Project</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="portfolio-modal modal fade" id="serviceModal4" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-12 mx-auto">
                <div class="modal-body">
                 <iframe width="300" height="200" src="https://www.youtube.com/embed/lc5E2oC0Ab8" frameborder="0" allowfullscreen></iframe>
                 <iframe width="300" height="200" src="https://www.youtube.com/embed/lc5E2oC0Ab8" frameborder="0" allowfullscreen></iframe>
                 <iframe width="300" height="200" src="https://www.youtube.com/embed/lc5E2oC0Ab8" frameborder="0" allowfullscreen></iframe>
                 <iframe width="300" height="200" src="https://www.youtube.com/embed/lc5E2oC0Ab8" frameborder="0" allowfullscreen></iframe>
                 <iframe width="300" height="200" src="https://www.youtube.com/embed/lc5E2oC0Ab8" frameborder="0" allowfullscreen></iframe>
                 <iframe width="300" height="200" src="https://www.youtube.com/embed/lc5E2oC0Ab8" frameborder="0" allowfullscreen></iframe>
                 <br>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fas fa-times"></i>
                    Close Project</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Portfolio Modals -->

    <!-- Modal 1 -->
    <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2 class="text-uppercase">Project Name</h2>
                  <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                  <img class="img-fluid d-block mx-auto" src="./img/portfolio/01-full.jpg" alt="">
                  <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                  <ul class="list-inline">
                    <li>Date: January 2017</li>
                    <li>Client: Threads</li>
                    <li>Category: Illustration</li>
                  </ul>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fas fa-times"></i>
                    Close Project</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 2 -->
    <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2 class="text-uppercase">Project Name</h2>
                  <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                  <img class="img-fluid d-block mx-auto" src="./img/portfolio/02-full.jpg" alt="">
                  <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                  <ul class="list-inline">
                    <li>Date: January 2017</li>
                    <li>Client: Explore</li>
                    <li>Category: Graphic Design</li>
                  </ul>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fas fa-times"></i>
                    Close Project</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 3 -->
    <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2 class="text-uppercase">Project Name</h2>
                  <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                  <img class="img-fluid d-block mx-auto" src="./img/portfolio/03-full.jpg" alt="">
                  <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                  <ul class="list-inline">
                    <li>Date: January 2017</li>
                    <li>Client: Finish</li>
                    <li>Category: Identity</li>
                  </ul>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fas fa-times"></i>
                    Close Project</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 4 -->
    <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2 class="text-uppercase">Project Name</h2>
                  <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                 <iframe width="600" height="400" src="https://www.youtube.com/embed/lc5E2oC0Ab8" frameborder="0" allowfullscreen></iframe>
                  <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                  <ul class="list-inline">
                    <li>Date: January 2017</li>
                    <li>Client: Lines</li>
                    <li>Category: Branding</li>
                  </ul>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fas fa-times"></i>
                    Close Project</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 5 -->
    <div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2 class="text-uppercase">Project Name</h2>
                  <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                 <iframe width="600" height="400" src="https://www.youtube.com/embed/4044So6wlqo" frameborder="0" allowfullscreen></iframe>
                  <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                  <ul class="list-inline">
                    <li>Date: January 2017</li>
                    <li>Client: Southwest</li>
                    <li>Category: Website Design</li>
                  </ul>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fas fa-times"></i>
                    Close Project</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 6 -->
    <div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2 class="text-uppercase">Project Name</h2>
                  <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                  <iframe width="600" height="400" src="https://www.youtube.com/embed/TJaIXRNJD0E" frameborder="0" allowfullscreen></iframe>
                  <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                  <ul class="list-inline">
                    <li>Date: January 2017</li>
                    <li>Client: Window</li>
                    <li>Category: Photography</li>
                  </ul>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fas fa-times"></i>
                    Close Project</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="./vendor/jquery/jquery.min.js"></script>
    <script src="./vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="./vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="./js/jqBootstrapValidation.js"></script>
    <script src="./js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="./js/agency.min.js"></script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-122235756-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-122235756-1');
    </script>
    <script src="https://www.google.com/recaptcha/api.js" async defer>

    </script>

    {{-- <script type="text/javascript">
      (function() {
        window._pa = window._pa || {};
        // _pa.orderId = "myOrderId"; // OPTIONAL: attach unique conversion identifier to conversions
        // _pa.revenue = "19.99"; // OPTIONAL: attach dynamic purchase values to conversions
        // _pa.productId = "myProductId"; // OPTIONAL: Include product ID for use with dynamic ads
        var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.async = true;
        pa.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + "//tag.marinsm.com/serve/55ecc0923dcf05c42c000006.js";
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(pa, s);
      })();
    </script> --}}
<script type="text/javascript">
    $("#portfolioModal6").on('hidden.bs.modal', function (e) {
    $("#portfolioModal6 iframe").attr("src", $("#portfolioModal6 iframe").attr("src"));
    });
    $("#portfolioModal5").on('hidden.bs.modal', function (e) {
    $("#portfolioModal5 iframe").attr("src", $("#portfolioModal5 iframe").attr("src"));
    });
    $("#portfolioModal4").on('hidden.bs.modal', function (e) {
    $("#portfolioModal4 iframe").attr("src", $("#portfolioModal4 iframe").attr("src"));
    });
    $("#serviceModal1").on('hidden.bs.modal', function (e) {
    $("#serviceModal1 iframe").attr("src", $("#serviceModal1 iframe").attr("src"));
    });
    $("#serviceModal2").on('hidden.bs.modal', function (e) {
    $("#serviceModal2 iframe").attr("src", $("#serviceModal2 iframe").attr("src"));
    });
    $("#serviceModal3").on('hidden.bs.modal', function (e) {
    $("#serviceModal3 iframe").attr("src", $("#serviceModal3 iframe").attr("src"));
    });
    $("#serviceModal4").on('hidden.bs.modal', function (e) {
    $("#serviceModal4 iframe").attr("src", $("#serviceModal4 iframe").attr("src"));
    });
</script>    <!-- GOOGLE ANALTYICS -->

    {{-- <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-57527017-1', 'auto');
      ga('send', 'pageview');

    </script> --}}

      <script src="./dist/js/index.js" type="text/javascript" charset="utf-8"></script>
      {{-- <script>
        var host = window.location.host;
        if (host.substring(host.length - 3) === 'dev') {
          document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>');
        }
      </script> --}}
        <!-- Google Code for Remarketing Tag -->

    {{-- <script type="text/javascript"> --}}
    {{-- /* <![CDATA[ */
    // var google_conversion_id = 955794029;
    // var google_custom_params = window.google_tag_params;
    // var google_remarketing_only = true;
    /* ]]> */ --}}
    {{-- </script> --}}
    {{-- <noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/955794029/?guid=ON&amp;script=0"/>
    </div>
    </noscript> --}}

  </body>

</html>
